program storm;

uses
  Windows,
  SysUtils,
  Classes,

  APITypes in 'API\APITypes.pas',
  APICommon in 'API\APICommon.pas',

  lua52 in 'Lua\lua52.pas',
  luaShell in 'Lua\luaShell.pas',

  SDLApi in 'API\SDLApi.pas',
  LUAApi in 'API\LUAApi.pas',

  sdl in 'SDL\sdl.pas',
  sdl_image in 'SDL\sdl_image.pas',
  sdlsprites in 'SDL\sdlsprites.pas',
  sdlutils in 'SDL\sdlutils.pas',
  sdlmonofonts in 'SDL\sdlmonofonts.pas'
{$ifdef NOT_USED}
  XMLApi in 'API\XMLApi.pas',
  
  logger in 'SDL\logger.pas',
  libxmlparser in 'SDL\libxmlparser.pas',
  moduleloader in 'SDL\moduleloader.pas',
  registryuserpreferences in 'SDL\registryuserpreferences.pas',
  sdl_cpuinfo in 'SDL\sdl_cpuinfo.pas',
  sdl_gfx in 'SDL\sdl_gfx.pas',
  sdlgameinterface in 'SDL\sdlgameinterface.pas',
  sdli386utils in 'SDL\sdli386utils.pas',
  sdlinput in 'SDL\sdlinput.pas',
  sdlstreams in 'SDL\sdlstreams.pas',
  sdlticks in 'SDL\sdlticks.pas',
  sdlwindow in 'SDL\sdlwindow.pas',
  sfont in 'SDL\sfont.pas',
  userpreferences in 'SDL\userpreferences.pas',
  xplatformutils in 'SDL\xplatformutils.pas'
{$endif}
  ;

var
  (* Lua *)
  L: Plua_State = nil;
  StartScriptName: string;
begin
  (* Prepare some vars *)
  StartScriptName := ChangeFileExt(ParamStr(0), '.lua');
  if not FileExists(StartScriptName) then
    Exit;
  (* Create Lua interpreter *)
  L := luaL_newstate;
  if not Assigned(L) then
    Exit;

  try
    (* Register API *)
    RegisterLUAAPI(L);
    RegisterCommonAPI(L);
    RegisterSDLAPI(L);

    (* Load & run scripts *)
    luaL_dofile(L, PChar(StartScriptName));
    (* Nothing to do *)
  finally
    lua_close(L);
  end;
end.
