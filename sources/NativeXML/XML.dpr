library XML;

uses
  XMLShell in 'XMLShell.pas';

exports
  XMLOpenFile name 'XML_OpenFile',
  XMLOpenNode name 'XML_OpenNode',
  XMLGetAttr name 'XML_GetAttr',
  XMLGetAttrByNode name 'XML_GetAttrByNode',
  XMLGetSubsections name 'XML_GetSubsections',
  XMLGetSubsectionsByNode name 'XML_GetSubsectionsByNode',
  XMLReadInteger name 'XML_ReadInteger',
  XMLReadIntegerByNode name 'XML_ReadIntegerByNode',
  XMLReadBoolean name 'XML_ReadBoolean',
  XMLReadBooleanByNode name 'XML_ReadBooleanByNode',
  XMLReadString name 'XML_ReadString',
  XMLReadStringByNode name 'XML_ReadStringByNode',
  XMLclose name 'XML_Close';

begin
end.
 