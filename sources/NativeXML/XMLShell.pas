unit XMLShell;

interface

uses
  NativeXml, SysUtils;

type
  TXML_GetSubsectionsCallback = procedure (const AUserData: Pointer;
    const ANode: TXmlNode; const ASectionName: PChar);

function XMLOpenFile(const AFileName: PChar): TNativeXml; cdecl;
function XMLOpenNode(const AXMLFile: TNativeXml; const APath: PChar): TXmlNode; cdecl;
function XMLGetAttr(const AXMLFile: TNativeXml; const APath: PChar;
  const AAttrName: PChar): TsdAttribute; cdecl;
function XMLGetAttrByNode(const ANode: TXmlNode; const AAttrName: PChar): TsdAttribute; cdecl;

function XMLGetSubsections(const AXMLFile: TNativeXml; const APath: PChar;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean; cdecl;
function XMLGetSubsectionsByNode(const ANode: TXmlNode;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean; cdecl;

function XMLReadInteger(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Integer): Integer; cdecl;
function XMLReadIntegerByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Integer): Integer; cdecl;

function XMLReadBoolean(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Boolean): Boolean; cdecl;
function XMLReadBooleanByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Boolean): Boolean; cdecl;

function XMLReadString(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: PChar): PChar; cdecl;
function XMLReadStringByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: PChar): PChar; cdecl;

function XMLclose(const AXMLFile: TNativeXml): Boolean; cdecl;

implementation

var
  buff_str: string = '';

function XMLOpenFile(const AFileName: PChar): TNativeXml;
begin
  Result := nil;
  if not FileExists(AFileName) then
    Exit;
    
  Result := TNativeXml.Create(nil);
  Result.LoadFromFile(AFileName);
end;

function XMLOpenNode(const AXMLFile: TNativeXml; const APath: PChar): TXmlNode;
var
  path: string;
  a, b, i: Integer;
  s: string;
begin
  Result := nil;
  path := APath;
  if path[length(path)] <> '\' then
  begin
    Result := XMLOpenNode(AXMLFile, PChar(path + '\'));
    Exit;
  end;
  
  a := 1; 
    
  for i := 1 to Length(path) do
    if path[i] = '\' then
    begin
      b := i;

      s := Copy(path, a, b-a);
      if Result = nil then
        Result := AXMLFile.Root.NodeByName(s)
      else
        Result := Result.NodeByName(s);

      if Result = nil then
        Exit;
      
      a := i+1;
    end;
end;

function XMLGetAttr(const AXMLFile: TNativeXml; const APath: PChar;
  const AAttrName: PChar): TsdAttribute;
var
  node: TXmlNode;    
begin
  Result := nil;

  node := XMLOpenNode(AXMLFile, APath);
  if node <> nil then
    Result := node.AttributeByName[AAttrName];
end;

function XMLGetAttrByNode(const ANode: TXmlNode; const AAttrName: PChar): TsdAttribute;
begin
  Result := nil;
  if ANode <> nil then
    Result := ANode.AttributeByName[AAttrName];
end;

function XMLGetSubsections(const AXMLFile: TNativeXml; const APath: PChar;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean;
var
  node: TXmlNode;
begin
  Result := False;

  node := XMLOpenNode(AXMLFile, APath);
  if node <> nil then
    Result := XMLGetSubsectionsByNode(node, AListCallback, AUserData, ARecursive);
end;

function XMLGetSubsectionsByNode(const ANode: TXmlNode;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean;
var
  i: Integer;
begin
  Result := False;

  if (ANode = nil) or (ANode.NodeCount = 0) then
    Exit;

  for i := 0 to ANode.NodeCount - 1 do
    if ANode.Nodes[i] is TsdElement then
    begin
      if Assigned(AListCallback) then
        AListCallback(AUserData, ANode.Nodes[i], PChar(ANode.Nodes[i].Name)); 

      if ARecursive then
        XMLGetSubsectionsByNode(ANode.Nodes[i], AListCallback, AUserData,
          ARecursive);
    end;

  Result := True;
end;

function XMLReadInteger(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Integer): Integer;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttr(AXMLFile, APath, AIdent);
  if attr = nil then
    Result := ADefVal
  else
    Result := attr.ValueAsInteger;
end;

function XMLReadIntegerByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Integer): Integer;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttrByNode(ANode, AIdent);
  if attr = nil then
    Result := ADefVal
  else
    Result := attr.ValueAsInteger;
end;

function XMLReadBoolean(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Boolean): Boolean;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttr(AXMLFile, APath, AIdent);
  if attr = nil then
    Result := ADefVal
  else
    Result := attr.ValueAsBool;
end;

function XMLReadBooleanByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Boolean): Boolean;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttrByNode(ANode, AIdent);
  if attr = nil then
    Result := ADefVal
  else
    Result := attr.ValueAsBool;
end;  

function XMLReadString(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: PChar): PChar;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttr(AXMLFile, APath, AIdent);
  if attr = nil then
    buff_str := ADefVal
  else
    buff_str := attr.Value;

  Result := PChar(buff_str);
end;

function XMLReadStringByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: PChar): PChar;
var
  attr: TsdAttribute;
begin
  attr := XMLGetAttrByNode(ANode, AIdent);
  if attr = nil then
    buff_str := ADefVal
  else
    buff_str := attr.Value;

  Result := PChar(buff_str);
end;

function XMLclose(const AXMLFile: TNativeXml): Boolean;
begin
  Result := False;

  if AXMLFile = nil then
    Exit;

  AXMLFile.Free;

  Result := True;
end;

end.
