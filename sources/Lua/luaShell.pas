unit luaShell;

interface

uses
  lua52;

function lua_CallProc(const ALua: Plua_State; const AProcName: PChar;
  const AArgs: array of const; const AResultCount: Integer;
  out AErrorMessage: string): Boolean;

function lua_ProcExists(const ALua: Plua_State; const AProcName: PChar): Boolean; overload;
function lua_ProcExists(const ALua: Plua_State; const AProcNames: array of PChar;
  out AErrorMessage: string): Boolean; overload;

implementation

uses SysUtils;

function lua_CallProc(const ALua: Plua_State; const AProcName: PChar;
  const AArgs: array of const; const AResultCount: Integer;
  out AErrorMessage: string): Boolean;
var
  i: Integer;  
begin
  Result := False;

  lua_getglobal(ALua, AProcName);

  i := lua_gettop(ALua);
  if lua_isnil(ALua, i) then
  begin
    lua_remove(ALua, i);
    AErrorMessage := Format('Function "%s" not found', [AProcName]);
    Exit;
  end;

  for i := 0 to Length(AArgs) - 1 do
    case AArgs[i].VType of
      vtInteger : lua_pushinteger (ALua, AArgs[i].VInteger);
      vtBoolean : lua_pushboolean (ALua, AArgs[i].VBoolean);
      vtExtended: lua_pushnumber  (ALua, AArgs[i].VExtended^);
      vtPChar   : lua_pushstring  (ALua, AArgs[i].VPChar);
      vtAnsiString,
      vtString  : lua_pushstring  (ALua, AArgs[i].VPChar);
      vtObject,
      vtPointer : lua_pushunsigned(ALua, lua_Unsigned(AArgs[i].VPointer));
//    else
      // error
    end;

  i := lua_pcall(ALua, length(AArgs), AResultCount, 0);
  Result := (i = LUA_OK);
  if not Result then
    AErrorMessage := Format('Function "%s" execution failure (code %d)', [AProcName, i]);

  i := lua_gettop(ALua);
  Result := Result and (i = AResultCount);
  if not Result then
    AErrorMessage := Format('Function "%s" return unexpected results count (%d expected; %d found)',
      [AProcName, AResultCount, i]);
end;

function lua_ProcExists(const ALua: Plua_State; const AProcName: PChar): Boolean;
var
  i: Integer;
begin
  lua_getglobal(ALua, AProcName);

  i := lua_gettop(ALua);
  Result := not lua_isnil(ALua, i);
  lua_remove(ALua, i);
end;

function lua_ProcExists(const ALua: Plua_State; const AProcNames: array of PChar;
  out AErrorMessage: string): Boolean;
var
  i: Integer;  
begin
  Result := False;
  for i := 0 to Length(AProcNames) - 1 do
    if not lua_ProcExists(ALua, AProcNames[i]) then
    begin
      AErrorMessage := Format('Function "%s" not found', [AProcNames[i]]);
      Exit;
    end;

  Result := True;
end;

end.
