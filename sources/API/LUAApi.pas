unit LUAApi;

interface

uses
  lua52;

procedure RegisterLUAAPI(const L: Plua_State);  

implementation

uses
  APITypes;

function LUADoFile(L: Plua_State): Integer; cdecl;
var
  fileName: string;
  reslt: Integer;
begin
  fileName := lua_tostring(L, 1);
  reslt := luaL_dofile(L, PChar(fileName));
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function LUADoString(L: Plua_State): Integer; cdecl;
var
  str: string;
  reslt: Integer;
begin
  str := lua_tostring(L, 1);
  reslt := luaL_dostring(L, PChar(str));
  lua_pushinteger(L, reslt);
  Result := 1;
end;

const
  API_LUADoFile = API_PREFIX + 'DoFile';
  API_LUAInclude = API_PREFIX + 'Include';
  API_LUADoString = API_PREFIX + 'DoString';

const
  APICount = 3;

  APIFuncs: array[0..APICount] of luaL_Reg = (
    (name: API_LUADoFile;
     func: LUADoFile),
    (name: API_LUAInclude;
     func: LUADoFile),
    (name: API_LUADoString;
     func: LUADoString),
    (name: nil;
     func: nil)
  );

procedure RegisterLUAAPI(const L: Plua_State);
const
  LIB_NAME = 'lua';    
begin
  luaL_newmetatable(L, LIB_NAME);
  luaL_newlib(L, @APIFuncs);
  lua_setglobal(L, LIB_NAME);
end;

end.
