unit SDLApi;

interface

uses
  lua52;

procedure RegisterSDLAPI(const L: Plua_State);

implementation

uses
  luaShell, APITypes,
  sdl, sdlsprites, sdl_image, sdlmonofonts, Math, SysUtils;

(* SDL General functions *)
function Init(L: Plua_State): Integer; cdecl;
var
  flags: UInt32;
  reslt: Integer;
begin
  flags := lua_tounsigned(L, 1);
  reslt := SDL_Init(flags);

  lua_pushinteger(L, reslt);
  Result := 1;
end;

function Quit(L: Plua_State): Integer; cdecl;
begin
  SDL_Quit;
  Result := 0;
end;

function GetTicks(L: Plua_State): Integer; cdecl;
var
  reslt: UInt32;
begin
  reslt := SDL_GetTicks;
  lua_pushunsigned(L, lua_unsigned(reslt));

  Result := 1;
end;

function Delay(L: Plua_State): Integer; cdecl;
var
  msec: UInt32;
begin
  msec := lua_tounsigned(L, 1);
  SDL_Delay(msec);

  Result := 0;
end;

function Flip(L: Plua_State): Integer; cdecl;
var
  reslt: Integer;
  screen: PSDL_Surface;
begin
  screen := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := SDL_Flip(screen);
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function SetVideoMode(L: Plua_State): Integer; cdecl;
var
  width, height, bpp: Integer;
  flags: UInt32;
  reslt: PSDL_Surface;
begin
  width := lua_tointeger(L, 1);
  height := lua_tointeger(L, 2);
  bpp := lua_tointeger(L, 3);
  flags := lua_tounsigned(L, 4);
  
  reslt := SDL_SetVideoMode(width, height, bpp, flags);
  lua_pushunsigned(L, lua_unsigned(reslt));
  
  Result := 1;
end;

function CreateRGBSurface(L: Plua_State): Integer; cdecl;
var
  flags: UInt32;
  width, height, depth: Integer;
  RMask, GMask, BMask, AMask: UInt32;
  reslt: PSDL_Surface;
begin
  flags  := lua_tounsigned(L, 1);
  width  := lua_tointeger (L, 2);
  height := lua_tointeger (L, 3);
  depth  := lua_tointeger (L, 4);
  RMask  := lua_tounsigned(L, 5);
  GMask  := lua_tounsigned(L, 6);
  BMask  := lua_tounsigned(L, 7);
  AMask  := lua_tounsigned(L, 8);

  reslt := SDL_CreateRGBSurface(flags, width, height, depth, RMask, GMask, BMask, AMask);
  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

function PollEvent(L: Plua_State): Integer; cdecl;
var
  event: TSDL_Event;
  reslt: Integer;
begin
  reslt := SDL_PollEvent(@event);

  lua_pushinteger(L, reslt);
  lua_pushinteger(L, event.type_);
  Result := 2;

  case event.type_ of
    SDL_NOEVENT:;
    SDL_ACTIVEEVENT:
    begin
      lua_pushinteger(L, event.active.gain);
      lua_pushinteger(L, event.active.state);
      inc(Result, 2);
    end;
    SDL_KEYDOWN, SDL_KEYUP:
    begin
      lua_pushinteger(L, event.key.which);
      lua_pushinteger(L, event.key.state);
      lua_pushinteger(L, event.key.keysym.scancode);
      lua_pushinteger(L, event.key.keysym.sym);
      lua_pushinteger(L, event.key.keysym.modifier);
      lua_pushinteger(L, event.key.keysym.unicode);
      inc(Result, 6);
    end;
    SDL_MOUSEMOTION:
    begin
      lua_pushinteger(L, event.motion.which);
      lua_pushinteger(L, event.motion.state);
      lua_pushinteger(L, event.motion.x);
      lua_pushinteger(L, event.motion.y);
      lua_pushinteger(L, event.motion.xrel);
      lua_pushinteger(L, event.motion.yrel);
      inc(Result, 6);
    end;
    SDL_MOUSEBUTTONDOWN, SDL_MOUSEBUTTONUP:
    begin
      lua_pushinteger(L, event.button.which);
      lua_pushinteger(L, event.button.button);
      lua_pushinteger(L, event.button.state);
      lua_pushinteger(L, event.button.x);
      lua_pushinteger(L, event.button.y);
      inc(Result, 5);
    end;
    // �������� ��������� ���������
//    SDL_JOYAXISMOTION:
//    SDL_JOYBALLMOTION:
//    SDL_JOYHATMOTION:
//    SDL_JOYBUTTONDOWN, SDL_JOYBUTTONUP:
    SDL_VIDEORESIZE:
    begin
      lua_pushinteger(L, event.resize.w);
      lua_pushinteger(L, event.resize.h);
      inc(Result, 2);
    end;
    SDL_QUITEV:;
    SDL_USEREVENT :
    begin
      lua_pushinteger(L, event.user.code);
      lua_pushunsigned(L, Cardinal(event.user.data1));
      lua_pushunsigned(L, Cardinal(event.user.data2));
      inc(Result, 3);
    end;
    SDL_SYSWMEVENT:
    begin
      lua_pushinteger(L, event.syswm.msg^.h_wnd);
      lua_pushinteger(L, event.syswm.msg^.msg);
      lua_pushinteger(L, event.syswm.msg^.w_Param);
      lua_pushinteger(L, event.syswm.msg^.lParam);
      inc(Result, 4);
    end;
  end;
end;

function WMSetCaption(L: Plua_State): Integer; cdecl;
var
  title, icon: string;
  ptitle, picon: PChar;
begin         
  title   := lua_tostring(L, 1);
  icon    := lua_tostring(L, 2);
  ptitle  := nil; picon   := nil;
  
  if title <> EmptyStr then ptitle := PChar(title);
  if icon <> EmptyStr then picon := PChar(icon);

  SDL_WM_SetCaption(ptitle, picon);

  Result := 0;
end;

function WMSetIcon(L: Plua_State): Integer; cdecl;
var
  icon: PSDL_Surface;
  mask: Byte;
begin
  icon := PSDL_Surface(lua_tounsigned(L, 1));
  mask := Byte(lua_tointeger(L, 2));
  SDL_WM_SetIcon(icon, mask);
  
  Result := 0;
end;

function ShowSDLCursor(L: Plua_State): Integer; cdecl;
var
  toggle: Integer;
begin
  toggle := lua_tointeger(L, 1);
  SDL_ShowCursor(toggle);
  
  Result := 0;
end;

function LoadBMP(L: Plua_State): Integer; cdecl;
var
  fName: string;
  reslt: PSDL_Surface;
begin
  fName := lua_tostring(L, 1);
  reslt := SDL_LoadBMP(PChar(fName));
  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

function DisplayFormat(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: PSDL_Surface;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := SDL_DisplayFormat(surface);
  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;
  
function SetColorKey(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: Integer;
  flag, key: UInt32;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  flag := lua_tounsigned(L, 2);
  key := lua_tounsigned(L, 3);
  reslt := SDL_SetColorKey(surface, flag, key);
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function SetAlpha(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: Integer;
  flag, alpha: UInt32;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  flag := lua_tounsigned(L, 2);
  alpha := Byte(lua_tointeger(L, 3));
  reslt := SDL_SetAlpha(surface, flag, alpha);
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function FreeSurface(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  SDL_FreeSurface(surface);
  Result := 0;
end;

function SetColors(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  colors: TSDL_Color;
  firstColor, nColors: Integer;
  reslt: Integer;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  colors.r := Byte(lua_tointeger(L, 2));
  colors.g := Byte(lua_tointeger(L, 3));
  colors.b := Byte(lua_tointeger(L, 4));
  firstColor := lua_tointeger(L, 5);
  nColors := lua_tointeger(L, 6);
  reslt := SDL_SetColors(surface, @colors, firstColor, nColors);
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function MapRGB(L: Plua_State): Integer; cdecl;
var
  fmt: PSDL_PixelFormat;
  r, g, b: Byte;
  reslt: UInt32;
begin
  fmt := PSDL_PixelFormat(lua_tounsigned(L, 1));
  r := Byte(lua_tointeger(L, 2));
  g := Byte(lua_tointeger(L, 3));
  b := Byte(lua_tointeger(L, 4));
  reslt := SDL_MapRGB(fmt, r, g, b);
  lua_pushunsigned(L, reslt);
  Result := 1;
end;

function UpdateRect(L: Plua_State): Integer; cdecl;
var
  scr: PSDL_Surface;
  x, y: SInt32;
  w, h: UInt32;
begin
  scr := PSDL_Surface(lua_tounsigned(L, 1));
  x := lua_tointeger(L, 2);
  y := lua_tointeger(L, 3);
  w := lua_tounsigned(L, 4);
  h := lua_tounsigned(L, 5);
  SDL_UpdateRect(scr, x, y, w, h);
  Result := 0;
end;

function GetSurfaceFlags(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: UInt32;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := surface^.flags;
  lua_pushunsigned(L, reslt);
  Result := 1;
end;

function GetSurfaceFormat(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: PSDL_PixelFormat;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := surface^.format;
  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

function GetSurfaceWidth(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: Integer;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := surface^.w;
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function GetSurfaceHeight(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: Integer;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := surface^.h;
  lua_pushinteger(L, reslt);
  Result := 1;
end;

function GetSurfacePitch(L: Plua_State): Integer; cdecl;
var
  surface: PSDL_Surface;
  reslt: UInt16;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  reslt := surface^.pitch;
  lua_pushunsigned(L, reslt);
  Result := 1;
end;

function UpperBlit(L: Plua_State): Integer; cdecl;
var
  src, dst: PSDL_Surface;
  srcrect, dstrect: PSDL_Rect;
begin
  src := PSDL_Surface(lua_tounsigned(L, 1));
  srcrect := PSDL_Rect(lua_tounsigned(L, 2));
  
  dst := PSDL_Surface(lua_tounsigned(L, 3));
  dstrect := PSDL_Rect(lua_tounsigned(L, 4));

  SDL_UpperBlit(src, srcrect, dst, dstrect);
  Result := 0;
end;

function LowerBlit(L: Plua_State): Integer; cdecl;
var
  src, dst: PSDL_Surface;
  srcrect, dstrect: PSDL_Rect;
begin
  src := PSDL_Surface(lua_tounsigned(L, 1));
  srcrect := PSDL_Rect(lua_tounsigned(L, 2));
  
  dst := PSDL_Surface(lua_tounsigned(L, 3));
  dstrect := PSDL_Rect(lua_tounsigned(L, 4));

  SDL_LowerBlit(src, srcrect, dst, dstrect);
  Result := 0;
end;

(* Rects *)
function InitRect(L: Plua_State): Integer; cdecl;
var
  reslt: PSDL_Rect;
begin
  //GetMem(reslt, SizeOf(TSDL_Rect));
  New(reslt);
  if lua_gettop(L) = 4 then
  begin
    reslt^.x := lua_tointeger(L, 1);
    reslt^.y := lua_tointeger(L, 2);
    reslt^.w := lua_tounsigned(L, 3);
    reslt^.h := lua_tounsigned(L, 4);
  end;

  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

function ModifyRect(L: Plua_State): Integer; cdecl;
var
  rect: PSDL_Rect;
begin
  rect := PSDL_Rect(lua_tounsigned(L, 1));
  rect^.x := lua_tointeger(L, 2);
  rect^.y := lua_tointeger(L, 3);
  rect^.w := lua_tounsigned(L, 4);
  rect^.h := lua_tounsigned(L, 5);

  lua_pushunsigned(L, lua_Unsigned(rect));
  Result := 1;
end;

function DisposeRect(L: Plua_State): Integer; cdecl;
var
  rect: PSDL_Rect;
begin
  rect := PSDL_Rect(lua_tounsigned(L, 1));
  //FreeMem(rect);
  Dispose(rect);
  Result := 0;
end;

(* SDL Image *)
function IMGLoad(L: Plua_State): Integer; cdecl;
var
  fName: string;
  reslt: PSDL_Surface;
begin
  fName := lua_tostring(L, 1);
  reslt := IMG_Load(PChar(fName));
  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

(* SpriteEngine *)
function SpriteEngine_Create(L: Plua_State): Integer; cdecl;
var
  screen: PSDL_Surface;
  sprEngine: TSpriteEngine;
begin
  screen := PSDL_Surface(lua_tounsigned(L, 1));
  sprEngine := TSpriteEngine.Create(screen);
  lua_pushunsigned(L, lua_Unsigned(sprEngine));
  Result := 1;
end;

function SpriteEngine_Release(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  sprEngine.Free;
  Result := 0;
end;

function SpriteEngine_Clear(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  sprEngine.Clear;
  Result := 0;
end;

function SpriteEngine_SortSprites(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  sprEngine.SortSprites;
  Result := 0;
end;

function SpriteEngine_AddSprite(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
  item: TSprite;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  item := TSprite(lua_tounsigned(L, 2));
  sprEngine.AddSprite(item);
  Result := 0;
end;

function SpriteEngine_GetSprite(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
  i: Integer;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  i := lua_tointeger(L, 2);
  lua_pushunsigned(L, lua_Unsigned(sprEngine.Sprites[i]));
  Result := 1;
end;

function SpriteEngine_RemoveSprite(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
  item: TSprite;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  item := TSprite(lua_tounsigned(L, 2));
  sprEngine.RemoveSprite(item);
  Result := 0;
end;

function SpriteEngine_Move(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  sprEngine.Move;
  Result := 0;
end;

function SpriteEngine_Draw(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  sprEngine.Draw;
  Result := 0;
end;

function SpriteEngine_Surface(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
  surface: PSDL_Surface; // for debug
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  surface := sprEngine.Surface;

  if lua_gettop(L) = 2 then
  begin
    surface := PSDL_Surface(lua_tounsigned(L, 2));
    sprEngine.Surface := surface;
    
    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(surface));

    Result := 1;
  end;
end;

function SpriteEngine_BackgroundSurface(L: Plua_State): Integer; cdecl;
var
  sprEngine: TSpriteEngine;
  backgroundSurface: PSDL_Surface; // for debug
begin
  sprEngine := TSpriteEngine(lua_tounsigned(L, 1));
  backgroundSurface := sprEngine.BackgroundSurface;

  if lua_gettop(L) = 2 then
  begin
    backgroundSurface := PSDL_Surface(lua_tounsigned(L, 2));
    sprEngine.backgroundSurface := backgroundSurface;
    
    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(backgroundSurface));

    Result := 1;
  end;
end;

(* Sprite *)
type
  TMadSprite = class(TSprite)
  private
    FError: string;
    FL: Plua_State;
    FOnDraw,
    FOnMove,
    FOnGetCollisionRect: string;
  protected
    procedure GetCollisionRect(Rect: PSDL_Rect); override;
    procedure Draw; override;
    procedure Move; override;
  end;

{ TMadSprite }

procedure TMadSprite.Draw;
begin
  inherited;
  if FOnDraw <> '' then
    lua_CallProc(FL, PChar(FOnDraw), [Self], 0, FError);
end;

procedure TMadSprite.GetCollisionRect(Rect: PSDL_Rect);
begin
  inherited;

end;

procedure TMadSprite.Move;
begin
  inherited;
  if FOnMove <> '' then
    lua_CallProc(FL, PChar(FOnMove), [Self], 0, FError);
end;  

(* Sprite *)
function Sprite_Create(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  img: string;
  w, h: Integer;
  alpha: Boolean;
begin
  img := lua_tostring(L, 1);
  w := lua_tointeger(L, 2); 
  h := lua_tointeger(L, 3);
  alpha := lua_toboolean(L, 4);
  spr := TMadSprite.Create(img, w, h, alpha);
  TMadSprite(spr).FL := L;
  lua_pushunsigned(L, lua_Unsigned(spr));
  Result := 1;
end;

function Sprite_Release(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  spr.Free;
  Result := 0;
end;

function Sprite_ID(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  id: Byte;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  id := spr.ID;

  if lua_gettop(L) = 2 then
  begin
    id := Byte(lua_tounsigned(L, 2));
    spr.ID := id;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(id));

    Result := 1;
  end;
end;

function Sprite_ParentList(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  list: TSpriteList;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  list := spr.ParentList;

  if lua_gettop(L) = 2 then
  begin
    list := TSpriteList(lua_tounsigned(L, 2));
    spr.ParentList := list;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(list));

    Result := 1;
  end;
end;

function Sprite_AnimPhase(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  animPhase: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  animPhase := spr.AnimPhase;

  if lua_gettop(L) = 2 then
  begin
    animPhase := lua_tointeger(L, 2);
    spr.AnimPhase := animPhase;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, animPhase);

    Result := 1;
  end;
end;

function Sprite_NumberOfFrames(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  numberOfFrames: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  numberOfFrames := spr.numberOfFrames;

  if lua_gettop(L) = 2 then
  begin
    numberOfFrames := lua_tointeger(L, 2);
    spr.NumberOfFrames := numberOfFrames;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, numberOfFrames);

    Result := 1;
  end;
end;

function Sprite_X(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  x: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  x := spr.x;

  if lua_gettop(L) = 2 then
  begin
    x := lua_tointeger(L, 2);
    spr.x := x;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, x);

    Result := 1;
  end;
end;

function Sprite_Y(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  y: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  y := spr.y;

  if lua_gettop(L) = 2 then
  begin
    y := lua_tointeger(L, 2);
    spr.y := y;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, y);

    Result := 1;
  end;
end;

function Sprite_Z(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  z: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  z := spr.z;

  if lua_gettop(L) = 2 then
  begin
    z := lua_tointeger(L, 2);
    spr.z := z;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, z);

    Result := 1;
  end;
end;

function Sprite_W(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  w: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  w := spr.w;

  if lua_gettop(L) = 2 then
  begin
    w := lua_tointeger(L, 2);
    spr.w := w;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, w);

    Result := 1;
  end;
end;

function Sprite_H(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  h: Integer;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  h := spr.h;

  if lua_gettop(L) = 2 then
  begin
    h := lua_tointeger(L, 2);
    spr.h := h;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, h);

    Result := 1;
  end;
end;

function Sprite_Surface(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  surface: PSDL_Surface;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  surface := spr.Surface;

  if lua_gettop(L) = 2 then
  begin
    surface := PSDL_Surface(lua_tounsigned(L, 2));
    spr.surface := surface;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(surface));

    Result := 1;
  end;
end;

function Sprite_Background(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  background: PSDL_Surface;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  background := spr.Background;

  if lua_gettop(L) = 2 then
  begin
    background := PSDL_Surface(lua_tounsigned(L, 2));
    spr.background := background;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(background));

    Result := 1;
  end;
end;

function Sprite_Image(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  image: PSDL_Surface;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  image := spr.Image;

  if lua_gettop(L) = 2 then
  begin
    image := PSDL_Surface(lua_tounsigned(L, 2));
    spr.image := image;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, lua_Unsigned(image));

    Result := 1;
  end;
end;

function Sprite_SrcRect(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  srcRect: TSDL_Rect;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  srcRect := spr.SrcRect;

  if lua_gettop(L) = 5 then
  begin
    srcRect.x := lua_tointeger(L, 2);
    srcRect.y := lua_tointeger(L, 3);
    srcRect.w := lua_tointeger(L, 4);
    srcRect.h := lua_tointeger(L, 5);

    spr.srcRect := srcRect;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, srcRect.x);
    lua_pushinteger(L, srcRect.y);
    lua_pushinteger(L, srcRect.w);
    lua_pushinteger(L, srcRect.h);

    Result := 4;
  end;
end;

function Sprite_PrevRect(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  prevRect: TSDL_Rect;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  prevRect := spr.PrevRect;

  if lua_gettop(L) = 5 then
  begin
    prevRect.x := lua_tointeger(L, 2);
    prevRect.y := lua_tointeger(L, 3);
    prevRect.w := lua_tointeger(L, 4);
    prevRect.h := lua_tointeger(L, 5);

    spr.prevRect := prevRect;

    Result := 0;
  end else
  begin
    lua_pushinteger(L, prevRect.x);
    lua_pushinteger(L, prevRect.y);
    lua_pushinteger(L, prevRect.w);
    lua_pushinteger(L, prevRect.h);

    Result := 4;
  end;
end;

function Sprite_GetCollisionRect(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  rect: TSDL_Rect;
begin
  spr := TSprite(lua_tounsigned(L, 1));

  rect.x := lua_tointeger(L, 2);
  rect.y := lua_tointeger(L, 3);
  rect.w := lua_tointeger(L, 4);
  rect.h := lua_tointeger(L, 5);

  spr.GetCollisionRect(@rect);

  Result := 0;
end;

function Sprite_Draw(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  spr.Draw;

  Result := 0;
end;

function Sprite_Move(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  spr.Move;

  Result := 0;
end;

function Sprite_Kill(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  spr.Kill;

  Result := 0;
end;

function Sprite_OnDraw(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  onDraw: string;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  onDraw := TMadSprite(spr).FOnDraw;

  if lua_gettop(L) = 2 then
  begin
    onDraw := lua_tostring(L, 2);

    TMadSprite(spr).FOnDraw := onDraw;

    Result := 0;
  end else
  begin
    lua_pushstring(L, onDraw);

    Result := 1;
  end;
end;

function Sprite_OnMove(L: Plua_State): Integer; cdecl;
var
  spr: TSprite;
  onMove: string;
begin
  spr := TSprite(lua_tounsigned(L, 1));
  onMove := TMadSprite(spr).FOnMove;

  if lua_gettop(L) = 2 then
  begin
    onMove := lua_tostring(L, 2);

    TMadSprite(spr).FOnMove := onMove;

    Result := 0;
  end else
  begin
    lua_pushstring(L, onMove);

    Result := 1;
  end;
end;

(* Font *)
function Font_Create(L: Plua_State): Integer; cdecl;
var
  reslt: PFont;
  surface: PSDL_Surface;
  fName: string;
begin
  surface := PSDL_Surface(lua_tounsigned(L, 1));
  fName := lua_tostring(L, 2);

  New(reslt, Initialize(fName));
  reslt^.Surface := surface;

  lua_pushunsigned(L, lua_Unsigned(reslt));
  Result := 1;
end;

function Font_Release(L: Plua_State): Integer; cdecl;
var
  fnt: PFont;
begin
  fnt := PFont(lua_tounsigned(L, 1));
  fnt^.Finalize;

  Result := 0;
end;

function Font_TextColor(L: Plua_State): Integer; cdecl;
var
  fnt: PFont;
  clr: Cardinal;
begin
  fnt := PFont(lua_tounsigned(L, 1));

  clr := fnt^.TextColor;
  if lua_gettop(L) = 2 then
  begin
    clr := lua_tounsigned(L, 2);

    fnt^.TextColor := clr;

    Result := 0;
  end else
  begin
    lua_pushunsigned(L, clr);

    Result := 1;
  end;
end;

function Font_LoadFont(L: Plua_State): Integer; cdecl;
var
  fnt: PFont;
  fName: string;
begin
  fnt := PFont(lua_tounsigned(L, 1));
  fName := lua_tostring(L, 2);
  fnt^.LoadFont(fName);

  Result := 0;
end;

function Font_WriteText(L: Plua_State): Integer; cdecl;
var
  fnt: PFont;
  x, y: Integer;
  txt: string;
  align: TAlignment;
begin
  fnt := PFont(lua_tounsigned(L, 1));
  x := lua_tointeger(L, 2);
  y := lua_tointeger(L, 3);
  txt := lua_tostring(L, 4);
  align := TAlignment(lua_tointeger(L, 5));
  fnt^.WriteText(x, y, PChar(txt), align);

  Result := 0;
end;

const
  API_Init = API_PREFIX + 'Init';
  API_Quit = API_PREFIX + 'Quit';
  API_GetTicks = API_PREFIX + 'GetTicks';
  API_Delay = API_PREFIX + 'Delay';
  API_Flip = API_PREFIX + 'Flip';
  API_SetVideoMode = API_PREFIX + 'SetVideoMode';
  API_CreateRGBSurface = API_PREFIX + 'CreateRGBSurface';
  API_PollEvent = API_PREFIX + 'PollEvent';
  API_SDLWMSetCaption = API_PREFIX + 'WMSetCaption';
  API_SDLWMSetIcon = API_PREFIX + 'WMSetIcon';
  API_ShowSDLCursor = API_PREFIX + 'ShowCursor';
  API_LoadBMP = API_PREFIX + 'LoadBMP';
  API_DisplayFormat = API_PREFIX + 'DisplayFormat';
  API_SetColorKey = API_PREFIX + 'SetColorKey';
  API_SetAlpha = API_PREFIX + 'SetAlpha';
  API_FreeSurface = API_PREFIX + 'FreeSurface';
  API_SetColors = API_PREFIX + 'SetColors';
  API_MapRGB = API_PREFIX + 'MapRGB';
  API_UpdateRect = API_PREFIX + 'UpdateRect';
  API_GetSurfaceFlags = API_PREFIX + 'GetSurfaceFlags';
  API_GetSurfaceFormat = API_PREFIX + 'GetSurfaceFormat';
  API_GetSurfaceWidth = API_PREFIX + 'GetSurfaceWidth';
  API_GetSurfaceHeight = API_PREFIX + 'GetSurfaceHeight';
  API_GetSurfacePitch = API_PREFIX + 'GetSurfacePitch';
  API_UpperBlit = API_PREFIX + 'UpperBlit';
  API_LowerBlit = API_PREFIX + 'LowerBlit';

  API_InitRect = API_PREFIX + 'Init';
  API_ModifyRect = API_PREFIX + 'Modify';
  API_DisposeRect = API_PREFIX + 'Dispose';

  API_IMGLoad = API_PREFIX + 'Load';

  API_SpriteEngine_Create = API_PREFIX + 'Create';
  API_SpriteEngine_Release = API_PREFIX + 'Free';
  API_SpriteEngine_Clear = API_PREFIX + 'Clear';
  API_SpriteEngine_SortSprites = API_PREFIX + 'SortSprites';
  API_SpriteEngine_AddSprite = API_PREFIX + 'AddSprite';
  API_SpriteEngine_GetSprite = API_PREFIX + 'GetSprite';
  API_SpriteEngine_RemoveSprite = API_PREFIX + 'RemoveSprite';
  API_SpriteEngine_Move = API_PREFIX + 'Move';
  API_SpriteEngine_Draw = API_PREFIX + 'Draw';
  API_SpriteEngine_Surface = API_PREFIX + 'Surface';
  API_SpriteEngine_BackgroundSurface = API_PREFIX + 'BackgroundSurface';
  
  API_Sprite_Create = API_PREFIX + 'Create';
  API_Sprite_Release = API_PREFIX + 'Free';
  API_Sprite_ID = API_PREFIX + 'ID';
  API_Sprite_ParentList = API_PREFIX + 'ParentList';
  API_Sprite_AnimPhase = API_PREFIX + 'AnimPhase';
  API_Sprite_NumberOfFrames = API_PREFIX + 'NumberOfFrames';
  API_Sprite_X = API_PREFIX + 'X';
  API_Sprite_Y = API_PREFIX + 'Y';
  API_Sprite_Z = API_PREFIX + 'Z';
  API_Sprite_W = API_PREFIX + 'W';
  API_Sprite_H = API_PREFIX + 'H';
  API_Sprite_Surface = API_PREFIX + 'Surface';
  API_Sprite_Background = API_PREFIX + 'Background';
  API_Sprite_Image = API_PREFIX + 'Image';
  API_Sprite_SrcRect = API_PREFIX + 'SrcRect';
  API_Sprite_PrevRect = API_PREFIX + 'PrevRect';
  API_Sprite_GetCollisionRect = API_PREFIX + 'GetCollisionRect';
  API_Sprite_Draw = API_PREFIX + 'Draw';
  API_Sprite_Move = API_PREFIX + 'Move';
  API_Sprite_Kill = API_PREFIX + 'Kill';
  API_Sprite_OnDraw = API_PREFIX + 'OnDraw';
  API_Sprite_OnMove = API_PREFIX + 'OnMove';
  
  API_Font_Create = API_PREFIX + 'Create';
  API_Font_Release = API_PREFIX + 'Free';
  API_Font_TextColor = API_PREFIX + 'TextColor';
  API_Font_LoadFont = API_PREFIX + 'LoadFont';
  API_Font_WriteText = API_PREFIX + 'WriteText';

const
  APISDLCount = 26;
  APISDL: array [0..APISDLCount] of luaL_Reg = (
    (name: API_Init;
     func: Init),
    (name: API_Quit;
     func: Quit),
    (name: API_GetTicks;
     func: GetTicks),
    (name: API_Delay;
     func: Delay),
    (name: API_Flip;
     func: Flip),
    (name: API_SetVideoMode;
     func: SetVideoMode),
    (name: API_CreateRGBSurface;
     func: CreateRGBSurface),
    (name: API_PollEvent;
     func: PollEvent),
    (name: API_SDLWMSetCaption;
     func: WMSetCaption),
    (name: API_SDLWMSetIcon;
     func: WMSetIcon),
    (name: API_ShowSDLCursor;
     func: ShowSDLCursor),
    (name: API_LoadBMP;
     func: LoadBMP),
    (name: API_DisplayFormat;
     func: DisplayFormat),
    (name: API_SetColorKey;
     func: SetColorKey),
    (name: API_SetAlpha;
     func: SetAlpha),
    (name: API_FreeSurface;
     func: FreeSurface),
    (name: API_SetColors;
     func: SetColors),
    (name: API_MapRGB;
     func: MapRGB),
    (name: API_UpdateRect;
     func: UpdateRect),
    (name: API_GetSurfaceFlags;
     func: GetSurfaceFlags),
    (name: API_GetSurfaceFormat;
     func: GetSurfaceFormat),
    (name: API_GetSurfaceWidth;
     func: GetSurfaceWidth),
    (name: API_GetSurfaceHeight;
     func: GetSurfaceHeight),
    (name: API_GetSurfacePitch;
     func: GetSurfacePitch),
    (name: API_UpperBlit;
     func: UpperBlit),
    (name: API_LowerBlit;
     func: LowerBlit),
    (name: nil;
     func: nil)
  );

  APIRectCount = 3;
  APIRect: array[0..APIRectCount] of luaL_Reg = (
    (name: API_InitRect;
     func: InitRect),
    (name: API_ModifyRect;
     func: ModifyRect),
    (name: API_DisposeRect;
     func: DisposeRect),
    (name: nil;
     func: nil)
  );

  APIImageCount = 1;
  APIImage: array[0..APIImageCount] of luaL_Reg = (
    (name: API_IMGLoad;
     func: IMGLoad),
    (name: nil;
     func: nil)
  );

  APIEngineCount = 11;
  APIEngine: array[0..APIEngineCount] of luaL_Reg = (
    (name: API_SpriteEngine_Create;
     func: SpriteEngine_Create),
    (name: API_SpriteEngine_Release;
     func: SpriteEngine_Release),
    (name: API_SpriteEngine_Clear;
     func: SpriteEngine_Clear),
    (name: API_SpriteEngine_SortSprites;
     func: SpriteEngine_SortSprites),
    (name: API_SpriteEngine_AddSprite;
     func: SpriteEngine_AddSprite),
    (name: API_SpriteEngine_GetSprite;
     func: SpriteEngine_GetSprite),
    (name: API_SpriteEngine_RemoveSprite;
     func: SpriteEngine_RemoveSprite),
    (name: API_SpriteEngine_Move;
     func: SpriteEngine_Move),
    (name: API_SpriteEngine_Draw;
     func: SpriteEngine_Draw),
    (name: API_SpriteEngine_Surface;
     func: SpriteEngine_Surface),
    (name: API_SpriteEngine_BackgroundSurface;
     func: SpriteEngine_BackgroundSurface),
    (name: nil;
     func: nil)
  );

  APISpriteCount = 22;
  APISprite: array[0..APISpriteCount] of luaL_Reg = (
    (name: API_Sprite_Create;
     func: Sprite_Create),
    (name: API_Sprite_Release;
     func: Sprite_Release),
    (name: API_Sprite_ID;
     func: Sprite_ID),
    (name: API_Sprite_ParentList;
     func: Sprite_ParentList),
    (name: API_Sprite_AnimPhase;
     func: Sprite_AnimPhase),
    (name: API_Sprite_NumberOfFrames;
     func: Sprite_NumberOfFrames),
    (name: API_Sprite_X;
     func: Sprite_X),
    (name: API_Sprite_Y;
     func: Sprite_Y),
    (name: API_Sprite_Z;
     func: Sprite_Z),
    (name: API_Sprite_W;
     func: Sprite_W),
    (name: API_Sprite_H;
     func: Sprite_H),
    (name: API_Sprite_Surface;
     func: Sprite_Surface),
    (name: API_Sprite_Background;
     func: Sprite_Background),
    (name: API_Sprite_Image;
     func: Sprite_Image),
    (name: API_Sprite_SrcRect;
     func: Sprite_SrcRect),
    (name: API_Sprite_PrevRect;
     func: Sprite_PrevRect),
    (name: API_Sprite_GetCollisionRect;
     func: Sprite_GetCollisionRect),
    (name: API_Sprite_Draw;
     func: Sprite_Draw),
    (name: API_Sprite_Move;
     func: Sprite_Move),
    (name: API_Sprite_Kill;
     func: Sprite_Kill),
    (name: API_Sprite_OnDraw;
     func: Sprite_OnDraw),
    (name: API_Sprite_OnMove;
     func: Sprite_OnMove),
    (name: nil;
     func: nil)
  );

  APIFontCount = 5;
  APIFont: array[0..APIFontCount] of luaL_Reg = (
    (name: API_Font_Create;
     func: Font_Create),
    (name: API_Font_Release;
     func: Font_Release),
    (name: API_Font_TextColor;
     func: Font_TextColor),
    (name: API_Font_LoadFont;
     func: Font_LoadFont),
    (name: API_Font_WriteText;
     func: Font_WriteText),
    (name: nil;
     func: nil)
  );

  APILibsCount = 6;
  APILibs: array[0..APILibsCount - 1] of TAPILibRec = (
    (LibName: 'sdl'     ; Reg: @APISDL),
    (LibName: 'rect'    ; Reg: @APIRect),
    (LibName: 'image'   ; Reg: @APIImage),
    (LibName: 'engine'  ; Reg: @APIEngine),
    (LibName: 'sprite'  ; Reg: @APISprite),
    (LibName: 'font'    ; Reg: @APIFont)
  );

procedure RegisterSDLAPI(const L: Plua_State);
var
  i: Integer;
begin
  i := 0;
  while i < APILibsCount do
  begin
    luaL_newmetatable(L, APILibs[i].LibName);
    luaL_newlib(L, APILibs[i].Reg);
    lua_setglobal(L, APILibs[i].LibName);

    inc(i);
  end;
end;

end.
