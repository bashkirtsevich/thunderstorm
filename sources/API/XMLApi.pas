unit XMLApi;

interface

uses
  Classes;

type
  TNativeXml = Pointer;
  TXmlNode = Pointer;
  TsdAttribute = Pointer;

function XMLOpenFile(const AFileName: string): TNativeXml;
function XMLOpenNode(const AXMLFile: TNativeXml; const APath: string): TXmlNode;
function XMLGetAttr(const AXMLFile: TNativeXml; const APath: string;
  const AAttrName: string): TsdAttribute; overload;
function XMLGetAttr(const ANode: TXmlNode; const AAttrName: string): TsdAttribute; overload;

function XMLGetSubsections(const AXMLFile: TNativeXml; const APath: string;
  const AList: TStrings; const ARecursive: Boolean = False): Boolean; overload;
function XMLGetSubsections(const ANode: TXmlNode;
  const AList: TStrings; const ARecursive: Boolean = False): Boolean; overload;

function XMLReadInteger(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: Integer): Integer; overload;
function XMLReadInteger(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: Integer): Integer; overload;

function XMLReadBoolean(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: Boolean): Boolean; overload;
function XMLReadBoolean(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: Boolean): Boolean; overload;

function XMLReadString(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: string): string; overload;
function XMLReadString(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: string): string; overload;

function XMLClose(const AXMLFile: TNativeXml): Boolean;

implementation

type
  TXML_GetSubsectionsCallback = procedure (const AUserData: Pointer;
    const ANode: TXmlNode; const ASectionName: PChar);

const
  XML_LIB_NAME = 'XML.dll';

function XML_OpenFile(const AFileName: PChar): TNativeXml; cdecl;
external XML_LIB_NAME name 'XML_OpenFile';

function XML_OpenNode(const AXMLFile: TNativeXml; const APath: PChar): TXmlNode; cdecl;
external XML_LIB_NAME name 'XML_OpenNode';

function XML_GetAttr(const AXMLFile: TNativeXml; const APath: PChar;
  const AAttrName: PChar): TsdAttribute; cdecl;
external XML_LIB_NAME name 'XML_GetAttr';

function XML_GetAttrByNode(const ANode: TXmlNode; const AAttrName: PChar): TsdAttribute; cdecl;
external XML_LIB_NAME name 'XML_GetAttrByNode';

function XML_GetSubsections(const AXMLFile: TNativeXml; const APath: PChar;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean; cdecl;
external XML_LIB_NAME name 'XML_GetSubsections';

function XML_GetSubsectionsByNode(const ANode: TXmlNode;
  const AListCallback: TXML_GetSubsectionsCallback;
  const AUserData: Pointer;
  const ARecursive: Boolean = False): Boolean; cdecl;
external XML_LIB_NAME name 'XML_GetSubsectionsByNode';

function XML_ReadInteger(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Integer): Integer; cdecl;
external XML_LIB_NAME name 'XML_ReadInteger';

function XML_ReadIntegerByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Integer): Integer; cdecl;
external XML_LIB_NAME name 'XML_ReadIntegerByNode';

function XML_ReadBoolean(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: Boolean): Boolean; cdecl;
external XML_LIB_NAME name 'XML_ReadBoolean';

function XML_ReadBooleanByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: Boolean): Boolean; cdecl;
external XML_LIB_NAME name 'XML_ReadBooleanByNode';

function XML_ReadString(const AXMLFile: TNativeXml; const APath: PChar;
  const AIdent: PChar; const ADefVal: PChar): PChar; cdecl;
external XML_LIB_NAME name 'XML_ReadString';

function XML_ReadStringByNode(const ANode: TXmlNode; const AIdent: PChar;
  const ADefVal: PChar): PChar; cdecl;
external XML_LIB_NAME name 'XML_ReadStringByNode';

function XML_Close(const AXMLFile: TNativeXml): Boolean; cdecl;
external XML_LIB_NAME name 'XML_Close';

procedure FillList(const AUserData: Pointer;
  const ANode: TXmlNode; const ASectionName: PChar);
var
  list: TStrings absolute AUserData;
begin
  list.AddObject(ASectionName, TObject(ANode));
end;

function XMLOpenFile(const AFileName: string): TNativeXml;
begin
  Result := XML_OpenFile(PChar(AFileName));
end;

function XMLOpenNode(const AXMLFile: TNativeXml; const APath: string): TXmlNode;
begin
  Result := XML_OpenNode(AXMLFile, PChar(APath));
end;

function XMLGetAttr(const AXMLFile: TNativeXml; const APath: string;
  const AAttrName: string): TsdAttribute;
begin
  Result := XML_GetAttr(AXMLFile, PChar(APath), PChar(AAttrName));
end;

function XMLGetAttr(const ANode: TXmlNode; const AAttrName: string): TsdAttribute;
begin
  Result := XML_GetAttrByNode(ANode, PChar(AAttrName));
end;

function XMLGetSubsections(const AXMLFile: TNativeXml; const APath: string;
  const AList: TStrings; const ARecursive: Boolean = False): Boolean;
begin
  Result := XML_GetSubsections(AXMLFile, PChar(APath), @FillList, AList, ARecursive);
end;

function XMLGetSubsections(const ANode: TXmlNode;
  const AList: TStrings; const ARecursive: Boolean = False): Boolean;
begin
  Result := XML_GetSubsectionsByNode(ANode, @FillList, AList, ARecursive);
end;

function XMLReadInteger(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: Integer): Integer;
begin
  Result := XML_ReadInteger(AXMLFile, PChar(APath), PChar(AIdent), ADefVal);
end;

function XMLReadInteger(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: Integer): Integer;
begin
  Result := XML_ReadIntegerByNode(ANode, PChar(AIdent), ADefVal);
end;

function XMLReadBoolean(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: Boolean): Boolean;
begin
  Result := XML_ReadBoolean(AXMLFile, PChar(APath), PChar(AIdent), ADefVal);
end;

function XMLReadBoolean(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: Boolean): Boolean;
begin
  Result := XML_ReadBooleanByNode(ANode, PChar(AIdent), ADefVal);
end;  

function XMLReadString(const AXMLFile: TNativeXml; const APath: string;
  const AIdent: string; const ADefVal: string): string;
begin
  Result := XML_ReadString(AXMLFile, PChar(APath), PChar(AIdent), PChar(ADefVal));
end;

function XMLReadString(const ANode: TXmlNode; const AIdent: string;
  const ADefVal: string): string;
begin
  Result := XML_ReadStringByNode(ANode, PChar(AIdent), PChar(ADefVal));
end;

function XMLClose(const AXMLFile: TNativeXml): Boolean;
begin
  Result := XML_Close(AXMLFile);
end;

end.