unit APITypes;

interface

uses
  lua52;

const
  API_PREFIX = '';

type
  TAPILibRec = packed record
    LibName: PAnsiChar;
    Reg: PluaL_Reg;
  end;

implementation

end.
