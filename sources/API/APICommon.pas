unit APICommon;

interface

uses
  lua52;

procedure RegisterCommonAPI(const L: Plua_State);

implementation

uses
  luaShell, APITypes, Math;

function BitAnd(L: Plua_State): Integer; cdecl;
var
  reslt: Cardinal;
  argCnt, i: Integer;
begin
  argCnt := lua_gettop(L);
  if argCnt >= 2 then
  begin
    reslt := lua_tounsigned(L, 1);
    for i := 2 to argCnt do
      reslt := reslt and lua_tounsigned(L, i);

    lua_pushunsigned(L, reslt);
    Result := 1;
  end else
    Result := 0;
end;

function BitOr(L: Plua_State): Integer; cdecl;
var
  reslt: Cardinal;
  argCnt, i: Integer;
begin
  argCnt := lua_gettop(L);
  if argCnt >= 2 then
  begin
    reslt := lua_tounsigned(L, 1);
    for i := 2 to argCnt do
      reslt := reslt or lua_tounsigned(L, i);

    lua_pushunsigned(L, reslt);
    Result := 1;
  end else
    Result := 0;
end;

function BitXor(L: Plua_State): Integer; cdecl;
var
  reslt: Cardinal;
  argCnt, i: Integer;
begin
  argCnt := lua_gettop(L);
  if argCnt >= 2 then
  begin
    reslt := lua_tounsigned(L, 1);
    for i := 2 to argCnt do
      reslt := reslt xor lua_tounsigned(L, i);

    lua_pushunsigned(L, reslt);
    Result := 1;
  end else
    Result := 0;
end;

function IntDiv(L: Plua_State): Integer; cdecl;
var
  reslt: Cardinal;
  a, b: Integer;
begin
  a := lua_tointeger(L, 1);
  b := lua_tointeger(L, 2);
  
  if b <> 0 then
    reslt := a div b
  else
    reslt := 0;

  lua_pushinteger(L, reslt);

  Result := 1;
end;

function IntMod(L: Plua_State): Integer; cdecl;
var
  reslt: Cardinal;
  a, b: Integer;
begin
  a := lua_tointeger(L, 1);
  b := lua_tointeger(L, 2);
  
  if b <> 0 then
    reslt := a mod b
  else
    reslt := 0;

  lua_pushinteger(L, reslt);

  Result := 1;
end;

function RandSeed_(L: Plua_State): Integer; cdecl;
var
  val: Integer;
begin
  if lua_gettop(L) = 0 then
  begin
    lua_pushinteger(L, RandSeed);
    Result := 1;
  end else
  begin
    val := lua_tointeger(L, 1);
    RandSeed := val;
    Result := 0;
  end;
end;

function Random_(L: Plua_State): Integer; cdecl;
var
  val: Integer;
begin
  if lua_gettop(L) = 0 then
    lua_pushnumber(L, Random)
  else
  begin
    val := lua_tointeger(L, 1);
    lua_pushinteger(L, Random(val));
  end;
  Result := 1;
end;

function Randomize_(L: Plua_State): Integer; cdecl;
begin
  Randomize;
  lua_pushinteger(L, RandSeed);
  Result := 1;
end;

function Min_(L: Plua_State): Integer; cdecl;
var
  i, valcnt, reslt, val: Integer;
begin
  valcnt := lua_gettop(L);
  if valcnt = 0 then
    reslt := Low(Integer)
  else
  if valcnt = 1 then
    reslt := lua_tointeger(L, 1)
  else
  begin
    reslt := High(Integer);
    for i := 1 to valcnt do
    begin
      val := lua_tointeger(L, i);
      if val < reslt then
        reslt := val;
    end;
  end;

  lua_pushinteger(L, reslt);
  Result := 1;
end;

function Max_(L: Plua_State): Integer; cdecl;
var
  i, valcnt, reslt, val: Integer;
begin
  valcnt := lua_gettop(L);
  if valcnt = 0 then
    reslt := High(Integer)
  else
  if valcnt = 1 then
    reslt := lua_tointeger(L, 1)
  else
  begin
    reslt := Low(Integer);
    for i := 1 to valcnt do
    begin
      val := lua_tointeger(L, i);
      if val > reslt then
        reslt := val;
    end;
  end;

  lua_pushinteger(L, reslt);
  Result := 1;
end;

function MinF_(L: Plua_State): Integer; cdecl;
var
  i, valcnt: Integer;
  reslt, val: Double;
begin
  valcnt := lua_gettop(L);
  if valcnt = 0 then
    reslt := NegInfinity
  else
  if valcnt = 1 then
    reslt := lua_tonumber(L, 1)
  else
  begin
    reslt := Infinity;
    for i := 1 to valcnt do
    begin
      val := lua_tointeger(L, i);
      if val < reslt then
        reslt := val;
    end;
  end;

  lua_pushnumber(L, reslt);
  Result := 1;
end;

function MaxF_(L: Plua_State): Integer; cdecl;
var
  i, valcnt: Integer;
  reslt, val: Double;
begin
  valcnt := lua_gettop(L);
  if valcnt = 0 then
    reslt := Infinity
  else
  if valcnt = 1 then
    reslt := lua_tonumber(L, 1)
  else
  begin
    reslt := NegInfinity;
    for i := 1 to valcnt do
    begin
      val := lua_tointeger(L, i);
      if val > reslt then
        reslt := val;
    end;
  end;

  lua_pushnumber(L, reslt);
  Result := 1;
end;

const
  API_BitAnd = API_PREFIX + 'BitAnd';
  API_BitOr = API_PREFIX + 'BitOr';
  API_BitXor = API_PREFIX + 'BitXor';
  API_IntDiv = API_PREFIX + 'IntDiv';
  API_IntMod = API_PREFIX + 'IntMod';
  API_RandSeed = API_PREFIX + 'RandSeed';
  API_Random = API_PREFIX + 'Random';
  API_Randomize = API_PREFIX + 'Randomize';
  API_Min = API_PREFIX + 'Min';
  API_Max = API_PREFIX + 'Max';
  API_MinF = API_PREFIX + 'MinF';
  API_MaxF = API_PREFIX + 'MaxF';

const
  APICount = 12;

  APIFuncs: array[0..APICount] of luaL_Reg = (
    (name: API_BitAnd;
     func: BitAnd),
    (name: API_BitOr;
     func: BitOr),
    (name: API_BitXor;
     func: BitXor),
    (name: API_IntDiv;
     func: IntDiv),
    (name: API_IntMod;
     func: IntMod),
    (name: API_RandSeed;
     func: RandSeed_),
    (name: API_Random;
     func: Random_),
    (name: API_Randomize;
     func: Randomize_),
    (name: API_Min;
     func: Min_),
    (name: API_Max;
     func: Max_),
    (name: API_MinF;
     func: MinF_),
    (name: API_MaxF;
     func: MaxF_),
    (name: nil;
     func: nil)
  );

procedure RegisterCommonAPI(const L: Plua_State);
const
  LIB_NAME = 'sys';    
begin
  luaL_newmetatable(L, LIB_NAME);
  luaL_newlib(L, @APIFuncs);
  lua_setglobal(L, LIB_NAME);
end;

end.