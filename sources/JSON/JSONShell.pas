unit JSONShell;

interface

uses
  uLkJSON, Classes, SysUtils;

implementation

function JSONCreate: TlkJSONobject; cdecl;
begin
  Result := TlkJSONobject.Create;
end;

function JSONOpenFile(const AFileName: PChar): TlkJSONobject; cdecl;
begin
  Result := nil;
  if not FileExists(AFileName) then
    Exit;

  Result := TlkJSONstreamed.LoadFromFile(AFileName) as TlkJSONobject;
end;

procedure JSONSaveFile(const AJSONObj: TlkJSONobject;
  const AFileName: PChar); cdecl;
begin
  TlkJSONstreamed.SaveToFile(AJSONObj, AFileName);
end;

function JSONAddStr(const AJSONObj: TlkJSONobject;
  const AName, AValue: PChar): Integer; cdecl;
begin
  Result := AJSONObj.Add(AName, string(AValue));
end;

function JSONAddInt(const AJSONObj: TlkJSONobject;
  const AName: PChar; const AValue: Integer): Integer; cdecl;
begin
  Result := AJSONObj.Add(AName, AValue);
end;

function JSONAddBool(const AJSONObj: TlkJSONobject;
  const AName: PChar; const AValue: Boolean): Integer; cdecl;
begin
  Result := AJSONObj.Add(AName, AValue);
end;

function JSONAddObj(const AJSONObj: TlkJSONobject;
  const AName: PChar; const AObj: TlkJSONbase): Integer; cdecl;
begin
  Result := AJSONObj.Add(AName, AObj);
end;

function JSONGetField(const AJSONObj: TlkJSONobject;
  const AField: PChar): TlkJSONbase; cdecl;
begin
  Result := AJSONObj.Field[string(AField)];
end;

end.
