-- data directory
WorkDir = "..\\data\\";

WindowTitle = "Thunderstorm";
WindowIconName = nil;

-- screen parameters
ScreenWidth = 800;
ScreenHeight = 600;
ScreenBPP = 32;

-- only loading functions
lua.DoFile(WorkDir.."lua\\consts.lua");
lua.DoFile(WorkDir.."lua\\cursor.lua");
lua.DoFile(WorkDir.."lua\\map.lua");
lua.DoFile(WorkDir.."lua\\menu.lua");
lua.DoFile(WorkDir.."lua\\buildings.lua");
lua.DoFile(WorkDir.."lua\\units.lua");
lua.DoFile(WorkDir.."lua\\testing.lua");
lua.DoFile(WorkDir.."lua\\main.lua");

main();