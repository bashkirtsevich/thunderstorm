function Testing_Main()
	sdl.ShowCursor(SDL_DISABLE);
	
	local tileset = {	TileHeight = 30,
						TileWidth = 60,
						ImagesCount = 1,
						FileNames = {"gfx\\grass.png"}
					};
	local map = CreateMap(30, tileset);
	
	local IsScrolling, CurPhase = false, CURSOR_SCROLL_PHASE_FREE;
	
	-------------------------------
	local tesla_trooper = NewUnit(TeslaTrooperInfo, 100, 100);
	-------------------------------
	
	SetCursor(CURSOR_ARROW);

	while Scene == SCENE_TESTING do
		while PollEvent() > 0 do
			if		Event.type_ == SDL_QUITEV then
				Scene = SCENE_NONE; break;
			elseif	Event.type_ == SDL_KEYDOWN and 
					Event.KeyboardEvent.keysym.sym == SDLK_ESCAPE then
				Scene = SCENE_NONE; break;
			elseif	Event.type_ == SDL_MOUSEMOTION 		or 
					Event.type_ == SDL_MOUSEBUTTONDOWN 	or 
					Event.type_ == SDL_MOUSEBUTTONUP 	then
				if 	Event.type_ == SDL_MOUSEBUTTONDOWN 	and 
					Event.MouseButtonEvent.button == SDL_BUTTON_RIGHT then 
					
					SetCursor(CURSOR_SCROLL);
					SetCursorPhase(CURSOR_SCROLL_PHASE_FREE);
					
					StartScrollMap(map, Event.MouseMotionEvent.x, 
										Event.MouseMotionEvent.y);
				elseif 	Event.type_ == SDL_MOUSEBUTTONUP and 
						Event.MouseButtonEvent.button == SDL_BUTTON_RIGHT then 
					SetCursor(CURSOR_ARROW); 
					
					StopScrollMap(map);
				end;
				
				MoveCursor(Event.MouseMotionEvent.x, Event.MouseMotionEvent.y);
				DoScrollMap(map, Event.MouseMotionEvent.x, 
								 Event.MouseMotionEvent.y, 
								 Screen.TickInterval);
			end;
		end;
		
		IsScrolling, CurPhase = DrawMap(map, BackgroundSurface, Screen.Height, Screen.Width);
		if IsScrolling then
			SetCursorPhase(CurPhase);
		end;
		
		engine.Move(SpriteEngine); -- move all sprites in the list
		engine.Draw(SpriteEngine); -- draw all sprites in the list
		
		sdl.UpdateRect(ScreenSurface, 0, 0, 0, 0);

		sdl.Flip(ScreenSurface);
		sdl.Delay(TimeLeft());
		
		sdl.UpperBlit(BackgroundSurface, nil, ScreenSurface, nil);
	end;
	
	DisposeMap(map);
	
	SetCursor(CURSOR_NONE);
end;