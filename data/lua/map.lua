function CreateMap(ASize, ATileSet)
	local result = {Scrolling = {	DoScroll = false, 
									x = 0, y = 0, 
									XScale = 0, YScale = 0,
									Left = 0, Top = 0
								},
					Surface = nil,
					Size = ASize,
					ScreenHeight = 0,
					ScreenWidth = 0,
					TilesHandles = {},
					Rect = nil
				};
	
	result.ScreenHeight = ASize * ATileSet.TileHeight;
	result.ScreenWidth 	= ASize * ATileSet.TileWidth;
	
	result.Surface = sdl.CreateRGBSurface(Screen.Flags, 
		result.ScreenWidth, 
		result.ScreenHeight, 
		Screen.Bpp, 
		0, 0, 0, 0);
	result.Surface = sdl.DisplayFormat(result.Surface);
	
	local i;
	for i = 1, ATileSet.ImagesCount do
		result.TilesHandles[i] = image.Load(WorkDir..ATileSet.FileNames[i]);
	end;
	
	result.Rect = rect.Init();
	local half_tileH, half_tileW = 	sys.IntDiv(ATileSet.TileHeight, 2), 
									sys.IntDiv(ATileSet.TileWidth , 2);
	local x, y;
	for x = 0, ASize-1 do
		result [x] = {};
		for y = 0, ASize-1 do
			result[x][y] = {x = (x+y), 
							y = (x-y), 
							z = 0,
							Building = nil
							};
			
			rect.Modify(result.Rect, 
						(x + y) * half_tileW, 
						(x - y) * half_tileH + ((ASize-1)*half_tileH), 
						0, 0);
			sdl.UpperBlit(result.TilesHandles[1], nil, result.Surface, result.Rect);
		end;
	end;
	
	-- temporary
	PlaceBuilding(result, 0, 0, TeslaGeneratorInfo, half_tileH, half_tileW);
	
	return result;
end;

function PlaceBuilding(AMap, AX, AY, ABuildingInfo, AHalfTileH, AHalfTileW)
	AMap[AX][AY].Building = NewBuilding(ABuildingInfo, 
										AMap[AX][AY].x * AHalfTileW, 
										AMap[AX][AY].y * AHalfTileH + AMap.Size*AHalfTileH);
end;

function StartScrollMap(AMap, AX, AY)
	AMap.Scrolling.DoScroll = true;
	
	AMap.Scrolling.x = AX;
	AMap.Scrolling.y = AY;
end;

function StopScrollMap(AMap)
	AMap.Scrolling.DoScroll = false;
	
	AMap.Scrolling.x = 0; 
	AMap.Scrolling.y = 0;
	
	AMap.Scrolling.XScale = 0;
	AMap.Scrolling.YScale = 0;
end;

function DoScrollMap(AMap, AX, AY, ATickInterval)
	if AMap.Scrolling.DoScroll then
		AMap.Scrolling.XScale = (AX - AMap.Scrolling.x) / ATickInterval;
		AMap.Scrolling.YScale = (AY - AMap.Scrolling.y) / ATickInterval;
	end;
end;

function DrawMap(AMap, ASurface, AWindowH, AWindowW)
	local result = CURSOR_SCROLL_PHASE_FREE;
	if AMap.Scrolling.DoScroll then
		AMap.Scrolling.Left = sys.Max(sys.Min(AMap.Scrolling.Left - AMap.Scrolling.XScale, 0), AWindowW-AMap.ScreenWidth);
		AMap.Scrolling.Top 	= sys.Max(sys.Min(AMap.Scrolling.Top  - AMap.Scrolling.YScale, 0), AWindowH-AMap.ScreenHeight);
		
		if 		(AMap.Scrolling.Left == 0							) 	and 
				(AMap.Scrolling.Top  == 0							) 	then 
			result = CURSOR_SCROLL_PHASE_NWLOCK;
		elseif	(AMap.Scrolling.Left == AWindowW-AMap.ScreenWidth	) 	and 
				(AMap.Scrolling.Top  == AWindowH-AMap.ScreenHeight	) 	then 
			result = CURSOR_SCROLL_PHASE_SELOCK;
		elseif	(AMap.Scrolling.Left == AWindowW-AMap.ScreenWidth	) 	and 
				(AMap.Scrolling.Top  == 0							) 	then 
			result = CURSOR_SCROLL_PHASE_NELOCK;
		elseif	(AMap.Scrolling.Left == 0							) 	and 
				(AMap.Scrolling.Top  == AWindowH-AMap.ScreenHeight	) 	then 
			result = CURSOR_SCROLL_PHASE_SWLOCK;
		elseif 	(AMap.Scrolling.Left == 0							) 	then 
			result = CURSOR_SCROLL_PHASE_WLOCK;
		elseif 	(AMap.Scrolling.Top  == 0							) 	then 
			result = CURSOR_SCROLL_PHASE_NLOCK;
		elseif 	(AMap.Scrolling.Left == AWindowW-AMap.ScreenWidth	) 	then 
			result = CURSOR_SCROLL_PHASE_ELOCK;
		elseif 	(AMap.Scrolling.Top  == AWindowH-AMap.ScreenHeight	) 	then 
			result = CURSOR_SCROLL_PHASE_SLOCK;
		end;
	end;
	
	rect.Modify(AMap.Rect, AMap.Scrolling.Left, AMap.Scrolling.Top, 0, 0);
	-- SDL_UpperBlit изменяет значения DstRect, если были отриц. значения - сбрасывает их в0
	sdl.UpperBlit(AMap.Surface, nil, ASurface, AMap.Rect);
	
	local x, y, building;
	for x = 0, AMap.Size-1 do
		for y = AMap.Size-1, 0, -1 do -- reverse loop
			building = AMap[x][y].Building;
			if (building ~= nil) then
				DrawBuilding(building, ASurface, AMap.Scrolling.Left, AMap.Scrolling.Top);
				AnimateBuilding(building);
			end;
		end;
	end;
	
	return AMap.Scrolling.DoScroll, result;
end;

function DisposeMap()

end;