CURSOR_NONE 	= 0;
CURSOR_ARROW 	= 1;
CURSOR_SCROLL 	= 2;

-- CURSOR_SCROLL phases
CURSOR_SCROLL_PHASE_FREE 	= 0;
CURSOR_SCROLL_PHASE_NLOCK 	= 1;
CURSOR_SCROLL_PHASE_NELOCK 	= 2;
CURSOR_SCROLL_PHASE_ELOCK 	= 3;
CURSOR_SCROLL_PHASE_SELOCK 	= 4;
CURSOR_SCROLL_PHASE_SLOCK 	= 5;
CURSOR_SCROLL_PHASE_SWLOCK 	= 6;
CURSOR_SCROLL_PHASE_WLOCK 	= 7;
CURSOR_SCROLL_PHASE_NWLOCK 	= 8;
--

local Cursors = {	CurrentCursor = CURSOR_NONE,
					Count 		= 2,
					FileNames 	= {	"gfx\\cursor_normal.png", 
									"gfx\\cursor_scroll.png"},
					FrameSizes	= {	{Width = 22, Height = 17},
									{Width = 55, Height = 43}},
					CurCenter 	= {	{x = 0, y = 0}, 
									{x = 28, y = 21}},
					Sprites 	= {},
					Phase		= 0,
					Engine 		= nil
				};

-- INITIALIZATION
function InitCursors(SpriteEngine)
	Cursors.Engine = SpriteEngine;
	for i = 1, Cursors.Count do
		Cursors.Sprites[i] = sprite.Create(	WorkDir..Cursors.FileNames[i], 
											Cursors.FrameSizes[i].Width, 
											Cursors.FrameSizes[i].Height, 
											true);
	end;
end;

-- FINALIZATION
function ReleaseCursors()
	engine.Clear(Cursors.Engine);
	for i = 1, Cursors.Count do
		sprite.Free(Cursors.Sprites[i]);
	end;
end;

function SetCursor(NewCursorIdx)
	if (Cursors.CurrentCursor ~= CURSOR_NONE) then
		engine.RemoveSprite(Cursors.Engine, Cursors.Sprites[Cursors.CurrentCursor]);
		if (NewCursorIdx == CURSOR_NONE) then
			return;
		end;
	end;
	Cursors.CurrentCursor = NewCursorIdx;
	Cursors.Phase = 0;
	engine.AddSprite(Cursors.Engine, Cursors.Sprites[Cursors.CurrentCursor]);
end;

function SetCursorPhase(NewPhase)
	if (NewPhase ~= Cursors.Phase) then
		Cursors.Phase = NewPhase;
		sprite.AnimPhase(Cursors.Sprites[Cursors.CurrentCursor], NewPhase);
	end;
end;

function MoveCursor(X, Y)
	if (Cursors.CurrentCursor ~= CURSOR_NONE) then
		sprite.X(Cursors.Sprites[Cursors.CurrentCursor], X - Cursors.CurCenter[Cursors.CurrentCursor].x);
		sprite.Y(Cursors.Sprites[Cursors.CurrentCursor], Y - Cursors.CurCenter[Cursors.CurrentCursor].y);
	end;
end;