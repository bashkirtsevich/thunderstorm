Screen = {	-- Window params
			Width 	= ScreenWidth,
			Height 	= ScreenHeight,
			Bpp 	= ScreenBPP,
			Flags 	= sys.BitOr(SDL_SWSURFACE, SDL_DOUBLEBUF, SDL_HWPALETTE), -- or SDL_FULLSCREEN
			-- FPS params
			TickInterval = sys.IntDiv(1000, 60), -- FPS
			NextTime = 0
		};
		
ScreenSurface 		= 0;
BackgroundSurface 	= 0;
SpriteEngine 		= 0;

Event = {	-- Event structure
			type_ = 0,
			ActiveEvent 		= {gain  = 0, state  = 0},
			KeyboardEvent 		= {which = 0, state  = 0, keysym  = {scancode = 0, sym  = 0, modifier = 0, unicode = 0}},
			MouseMotionEvent 	= {which = 0, state  = 0, x       = 0, y      = 0, xrel = 0, yrel     = 0},
			MouseButtonEvent 	= {which = 0, button = 0, state   = 0, x      = 0, y    = 0},
			ResizeEvent 		= {w     = 0, h      = 0},
			UserEvent 			= {code  = 0, data1  = 0, data2   = 0},
			SysWMEvent 			= {h_wnd = 0, msg    = 0, w_Param = 0, lParam = 0}
		};

function TimeLeft()
	local ticks = sdl.GetTicks();
	
	if (Screen.NextTime <= ticks) then
		Screen.NextTime = ticks + Screen.TickInterval;
		return 0;
	end;
	
	return (Screen.NextTime - ticks);
end;

function PollEvent()
	local result, type_, v1, v2, v3, v4, v5, v6 = sdl.PollEvent();
	
	Event.type_ = type_;
	if 		(type_ == SDL_ACTIVEEVENT) then
		Event.ActiveEvent 		= {gain = v1, state = v2};
	elseif	(type_ == SDL_KEYDOWN) or (type_ == SDL_KEYUP) then
		Event.KeyboardEvent		= {which = v1, state = v2, keysym = {scancode = v3, sym = v4, modifier = v5, unicode = v6}};
	elseif	(type_ == SDL_MOUSEMOTION) then
		Event.MouseMotionEvent 	= {which = v1, state = v2, x = v3, y = v4, xrel = v5, yrel = v6};
	elseif	(type_ == SDL_MOUSEBUTTONDOWN) or (type_ == SDL_MOUSEBUTTONUP) then
		Event.MouseButtonEvent 	= {which = v1, button = v2, state = v3, x = v4, y = v5};
	elseif	(type_ == SDL_VIDEORESIZE) then
		Event.ResizeEvent 		= {w = v1, h = v2};
	elseif	(type_ == SDL_USEREVENT) then
		Event.UserEvent 		= {code = v1, data1 = v2, data2 = v3};
	elseif	(type_ == SDL_SYSWMEVENT) then
		Event.SysWMEvent 		= {h_wnd = v1, msg = v2, w_Param = v3, lParam = v4};
	end;

	return result;
end;

-- Scene codes
SCENE_NONE = 0;
SCENE_MENU = 1;
SCENE_TESTING = 2;
-- Current scene
Scene = SCENE_NONE;

function main()
	-- Initialize
	if (sdl.Init(SDL_INIT_VIDEO) < 0) then return; end;
	-- Change window caption & set icon
	sdl.WMSetCaption(WindowTitle, WindowIconName);
	-- sdl.WMSetIcon(image.Load(""), 0);
	-- sdl.ShowCursor(SDL_DISABLE);
	
	ScreenSurface = sdl.SetVideoMode(Screen.Width, Screen.Height, Screen.Bpp, Screen.Flags);
	if (ScreenSurface == 0) then return; end; -- ����� �������� ��� ������
	
	BackgroundSurface = sdl.CreateRGBSurface(Screen.Flags, Screen.Width, Screen.Height, 
		Screen.Bpp, 0, 0, 0, 0);
	if (BackgroundSurface == 0) then return; end; -- ����� �������� ��� ������
	BackgroundSurface = sdl.DisplayFormat(BackgroundSurface);
	
	SpriteEngine = engine.Create(ScreenSurface);
	engine.BackgroundSurface(SpriteEngine, BackgroundSurface);
	
	-- init cursors
	InitCursors(SpriteEngine);
	
	Scene = SCENE_TESTING;
	
	repeat
		if 		(Scene == SCENE_MENU) then 
			Menu_Main();
		elseif 	(Scene == SCENE_TESTING) then 
			Testing_Main(); 
		end;
		
	until Scene == SCENE_NONE;
	
	ReleaseCursors();
	
	engine.Free(SpriteEngine);
	
	sdl.FreeSurface(ScreenSurface);
	sdl.Quit();
end;