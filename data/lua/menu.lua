function Menu_Main()
	--[[sdl.ShowCursor(SDL_DISABLE);
	local font0, font1 = font.Create(BackgroundSurface, WorkDir.."gfx\\font.png"), 
						font.Create(BackgroundSurface, WorkDir.."gfx\\font.png");
	font.TextColor(font0, sdl.MapRGB(sdl.GetSurfaceFormat(BackgroundSurface), 255, 0, 255));
	font.TextColor(font1, sdl.MapRGB(sdl.GetSurfaceFormat(BackgroundSurface), 255, 255, 255));
	
	local cursor = sprite.Create(WorkDir.."gfx\\cursor_normal.png", 17, 24, true);
	engine.AddSprite(SpriteEngine, cursor);
	
	local ImagesCount = 3;
	local ImageNamesList = {"gfx\\grass.png", "gfx\\brown.png", "gfx\\dirtpath.png"};
	local ImageList = {};
	for i = 1, ImagesCount do
		ImageList[i] = image.Load(WorkDir..ImageNamesList[i]);
		--[[sdl.SetColorKey(ImageList[i], 
						sys.BitOr(SDL_SRCCOLORKEY, SDL_RLEACCEL), 
						sdl.MapRGB(sdl.GetSurfaceFormat(ImageList[i]), 0, 0, 0));]]
	end;
	
	Rect, Rect1 = rect.Init(0, 0, 0, 0), rect.Init(10, 0, 0, 0);
	rect.Modify(Rect, 20, 30, 0, 0); rect.Modify(Rect1, 30, 30, 0, 0);]]
	
	while Scene == SCENE_MENU do
		-- temporary code
		while PollEvent() > 0 do
			if		Event.type_ == SDL_QUITEV then
				Scene = SCENE_NONE; break;
			elseif	Event.type_ == SDL_KEYDOWN and Event.KeyboardEvent.keysym.sym == SDLK_ESCAPE then
				Scene = SCENE_NONE; break;
			elseif	Event.type_ == SDL_MOUSEMOTION then
				-- just for test
				--[[sprite.X(cursor, Event.MouseMotionEvent.x);
				sprite.Y(cursor, Event.MouseMotionEvent.y);]]
			end;
		end;
		--[[
		font.WriteText(font0, 10, 10, "Testing", FONT_ALIGN_LEFT_JUSTIFY);
		font.WriteText(font1, 10, 30, "Testing", FONT_ALIGN_LEFT_JUSTIFY);
		
		sdl.UpperBlit(ImageList[1], nil, BackgroundSurface, Rect);
		sdl.UpperBlit(ImageList[2], nil, BackgroundSurface, Rect1);
		rect.Modify(Rect1, 30, 30, 0, 0);

		engine.Move(SpriteEngine); -- move all sprites in the list
		engine.Draw(SpriteEngine); -- draw all sprites in the list
		
		sdl.UpdateRect(ScreenSurface, 0, 0, 0, 0);
		--
		sdl.Flip(ScreenSurface);
		sdl.Delay(TimeLeft());
		
		sdl.UpperBlit(BackgroundSurface, nil, ScreenSurface, nil);]]
	end;
	
	--[[rect.Dispose(Rect); rect.Dispose(Rect1);
	
	engine.RemoveSprite(SpriteEngine, cursor);
	sprite.Free(cursor);
	
	font.Free(font0); font.Free(font1);]]
end;