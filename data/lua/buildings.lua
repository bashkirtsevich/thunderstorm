--[[
	--== TESLA COIL ==--
	tesla coil animation
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngtsla_a.shp
	tesla coil blowing up
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngtsladm.shp
	tesla coil buildings
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\isotemp\nttslamk.shp
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\isosnow\natslamk.shp
	
	--== TESLA GENERATOR ==--
	tesla generator buildings
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\isotemp\ntpowrmk.shp
	tesla generator animation
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngpowr_a.shp
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\snow\napowr_a.shp
	tesla generator offline
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngpowr.shp
	
	--== CONSTRUCTION YARD ==--
	construction yard "attack"
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngcnst_b.shp
	construction yard blowing up (коцаная картинка)
	G:\Command & Conquer\Red Alert 2 - Yuri's Revenge\ra2_data\ra2\generic\ngcnstdm.shp
]]

-- phases:
PHASE_BUILDING 		= 1;
PHASE_WORKING 		= 2;
PHASE_DAMAGED_WORK 	= 3;
PHASE_ATTACK		= 4;
PAHSE_DESTROYING 	= 5;

-- buildings info
TeslaGeneratorInfo = {
	SpriteH = 140,
	SpriteW = 210,
	RelX = 34,
	RelY = 104,
	Building = {FramesSount = 26,
				Sprites = "gfx\\tesla_generator_build.png"
				},
	WorkingN = {FramesSount = 19,
				Sprites = "gfx\\tesla_generator_normal_work.png"
				},
	WorkingD = {FramesSount = 19,
				Sprites = "gfx\\tesla_generator_broken_work.png"
				}
};

function NewBuilding(ABuildingInfo, AX, AY)
	local result = {Building = {Image 		= image.Load(WorkDir..ABuildingInfo.Building.Sprites),
								FramesCount = ABuildingInfo.Building.FramesSount,
								},
					WorkingN = {Image 		= image.Load(WorkDir..ABuildingInfo.WorkingN.Sprites),
								FramesCount = ABuildingInfo.WorkingN.FramesSount,
								},
					WorkingD = {Image 		= image.Load(WorkDir..ABuildingInfo.WorkingD.Sprites),
								FramesCount = ABuildingInfo.WorkingD.FramesSount,
								},
					Image 		= nil,
					FramesCount = 0,
					Frame 		= 0,
					FrameH 		= ABuildingInfo.SpriteH,
					FrameW 		= ABuildingInfo.SpriteW,
					FrameFreq 	= 10,
					FrameFreqIdx = 0,
					X 			= AX,
					Y 			= AY,
					RelX 		= ABuildingInfo.RelX,
					RelY 		= ABuildingInfo.RelY,
					Phase 		= PHASE_BUILDING
					};
	
	result.Image = result.Building.Image;
	result.FramesCount = result.Building.FramesCount;
	
	return result;
end;

-- нужно сделать вызов деструктора
local R1 = rect.Init();
local R2 = rect.Init();

function DrawBuilding(ABuildingInfo, ASurface, AMapLeft, AMapTop)
	rect.Modify(R1, 
				ABuildingInfo.FrameW * ABuildingInfo.Frame, 0, 
				ABuildingInfo.FrameW, 
				ABuildingInfo.FrameH);
	
	rect.Modify(R2, 
				ABuildingInfo.X + AMapLeft - ABuildingInfo.RelX, 
				ABuildingInfo.Y + AMapTop - ABuildingInfo.RelY, 
				ABuildingInfo.X + ABuildingInfo.FrameW, 
				ABuildingInfo.Y + ABuildingInfo.FrameH);
	
	sdl.UpperBlit(ABuildingInfo.Image, R1, ASurface, R2);
end;

function AnimateBuilding(ABuildingInfo)
	ABuildingInfo.FrameFreqIdx = ABuildingInfo.FrameFreqIdx + 1;
	
	if (ABuildingInfo.FrameFreqIdx >= ABuildingInfo.FrameFreq) then
		ABuildingInfo.FrameFreqIdx = 0;
		
		ABuildingInfo.Frame = ABuildingInfo.Frame+1;
		
		if (ABuildingInfo.Frame >= ABuildingInfo.FramesCount) then
			if 		(ABuildingInfo.Phase == PHASE_BUILDING) then
				ABuildingInfo.Phase = PHASE_WORKING;
				ABuildingInfo.Image = ABuildingInfo.WorkingN.Image;
				ABuildingInfo.FramesCount = ABuildingInfo.WorkingN.FramesCount;
				ABuildingInfo.Frame = 0;
			elseif	(ABuildingInfo.Phase == PHASE_WORKING) then
				ABuildingInfo.Frame = 0;
			end;
		end;
	end;
end;