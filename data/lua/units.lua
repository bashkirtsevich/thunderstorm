TeslaTrooperInfo = {
	SpriteH = 76,
	SpriteW = 66,
	SurfaceH = 1216,
	SurfaceW = 1320,
	RelX = 0,
	RelY = 0,
	FramesSount = 301,
	Sprites = "gfx\\tesla_trooper.png"
};

function NewUnit(AUnitInfo, AX, AY)
	local result = {Image 		= image.Load(WorkDir..AUnitInfo.Sprites),
					FramesCount = AUnitInfo.FramesSount,
					SurfaceH 	= AUnitInfo.SurfaceH,
					SurfaceW 	= AUnitInfo.SurfaceW,
					Frame 		= 0,
					FrameH 		= AUnitInfo.SpriteH,
					FrameW 		= AUnitInfo.SpriteW,
					FrameFreq 	= 10,
					FrameFreqIdx = 0,
					X 			= AX,
					Y 			= AY,
					RelX 		= AUnitInfo.RelX,
					RelY 		= AUnitInfo.RelY
					};
	
	return result;
end;

function DrawUnit(AUnitInfo)

end;

function AnimateUnit(AUnitInfo)

end;