RandSeed = 0;

function Random()
	local two2neg32 = ((1.0/0x10000) / 0x10000);  -- 2^-32
	local Temp;

	Temp = RandSeed * 0x08088405 + 1;
	if (Temp > 0xFFFFFFFF) then
		Temp = STORM_BitAnd(Temp, 0xFFFFFFFF);
	end;
	RandSeed = Temp;
	return Temp * two2neg32;
end;