-- Minimal portable random generator by Park and Miller 
ParkMillerRandSeed = 0;

local iy = 0;
local iv = {};

function ParkMillerRandom()
	-- Lewis-Goodman-Miller constants 
	local IA	= 16807;
	local IM	= 2147483647;
	local AM	= (1./IM);
	-- Scharge constants 
	local IQ	= 12773;
	local IR	= 2836;
	--
	local NTAB	= 32;
	local NWUP	= 8;
	local NDIV	= STORM_IntDiv(1+(IM-1), NTAB);
	local EPS	= 1.2e-7;
	local RNMX	= (1.0-EPS);
	local j, k, temp;

  -- initialize
	if (RandSeed <= 0) or (iy == 0) then
		-- avoid negative or zero seed 
		if		(RandSeed <  0) then RandSeed = -RandSeed; 
		elseif	(RandSeed == 0) then RandSeed = 1; end;

		-- after NWUP warmups, initialize shuffle table

		j = NTAB + NWUP - 1;
		while (j >= 0) do
			k = STORM_IntDiv(RandSeed, IQ);

			RandSeed = IA * (RandSeed - k * IQ) - IR * k;
			if (RandSeed < 0)	then RandSeed	= RandSeed + IM;	end;
			if (j < NTAB) 		then iv[j]		= RandSeed;			end;

			j = j - 1;
		end;

		-- first specimen from the table
		iy = iv[0];
	end;

	-- regular work: generate new number 
	k = STORM_IntDiv(RandSeed, IQ);
	RandSeed = IA * (RandSeed - k * IQ) - IR * k;
	if (RandSeed < 0) then RandSeed = RandSeed+ IM; end;

	-- shuffle output
	j     = STORM_IntDiv(iy, NDIV);
	iy    = iv[j];
	iv[j] = RandSeed;

	-- return
	temp = AM * iy;
	if (temp > RNMX) then 	return RNMX;
	else 					return temp; end;
end;