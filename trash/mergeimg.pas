var
  i: Integer;
  b: TBitmap;
begin
  img1.Height := 140;
  img1.Width := 19*210;
  for i := 20 to 38 do
  begin
    b := TBitmap.Create;
    try
      b.LoadFromFile(Format('g:\Command & Conquer\Red Alert 2 - Yuri''s Revenge\SHP_Test\dog\%d.bmp', [i]));
      img1.Canvas.Draw((i-20)*210, 0, b);
    finally
      b.Free;
    end;
  end;
  img1.Picture.SaveToFile('c:\1.bmp');